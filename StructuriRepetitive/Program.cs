﻿using System;

namespace StructuriRepetitive
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("While");
            int i = 0;

            while (i < 4)
            {
                i++;
                Console.WriteLine(i);
            }
            
            // ce val are i aici?
            

            Console.WriteLine("do - while");
            int j = 0;

            do
            {
                j++;
                Console.WriteLine(j);
            }
            while (j == 0);


            // scenariu do - while
            bool rezultat; // default val false
            int numar;
            
            do
            {
                Console.WriteLine("Introduceti un numar: ");
                rezultat = int.TryParse(Console.ReadLine(), out numar);
            }
            while (rezultat == false);

            Console.WriteLine("Numarul introdus este: " + numar);
            
            // exercitiu: sa limitam la 3 incercari

            // acelasi scenariu, de data asta folosind while
            int numar2;
            Console.WriteLine("Introduceti un numar: ");

            while (int.TryParse(Console.ReadLine(), out numar2) == false)
            {
                Console.WriteLine("Introduceti un numar: ");
            }
            

            Console.WriteLine("Numarul introdus este: " + numar2);


            // Structura de control FOR
            for (int limita = 1; limita <= 5; limita++)
            {
                Console.WriteLine(limita);
            }

            // variabila limita, nu mai este acesibila dupa ce FOR va termina executia

            int limita2;
            for (limita2 = 1; limita2 <= 5; limita2++)
            {
                Console.WriteLine(limita2);
            }

            Console.WriteLine(limita2);


            // sarim peste numere pare
            for (int limita = 1; limita <= 10; limita++)
            {
                if (limita % 2 == 0)
                    continue;

                Console.WriteLine(limita);
            }
            
            // ex: intr-un for preluam de la utilizator 5 numere si afisam suma lor
            // daca unul din numere este 13 sau multiplu de 13, oprim executia
            // si afisam urmatorul mesaj: Numar cu ghinion, ne oprim aici

            Console.ReadKey();
        }
    }
}